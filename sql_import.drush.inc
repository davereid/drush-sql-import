<?php

/**
 * Implements hook_drush_command().
 */
function sql_import_drush_command() {
  // Copied from sql_drush_command().
  $options['database'] = array(
    'description' => 'The DB connection key if using multiple connections in settings.php.',
    'example-value' => 'key',
  );
  $db_url['db-url'] = array(
    'description' => 'A Drupal 6 style database URL.',
    'example-value' => 'mysql://root:pass@127.0.0.1/db',
  );
  $options['target'] = array(
    'description' => 'The name of a target within the specified database connection. Defaults to \'default\'.',
    'example-value' => 'key',

    // Gets unhidden in help_alter(). We only want to show this to D7 users but have to
    // declare it here since some commands do not bootstrap fully.
    'hidden' => TRUE,
  );
  $items['sql-import'] = array(
    'description' => 'Import the Drupal DB from a SQL file.',
    'arguments' => array(
      'file' => array(
        'description' => 'The SQL file to import to the database. The file should be relative to Drupal root.',
        'example-value' => '/path/to/file',
      ),
    ),
    'required-arguments' => TRUE,
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'options' => array(
      'drop' => 'If provided will drop the database prior to import. Use with caution.',
      'yes' => 'Skip confirmation and proceed.',
    ) + $options + $db_url,
    'aliases' => array('sqli'),
    'topics' => array('docs-policy'),
    // Hack to make this command appear in the sql-import section.
    // @todo Change to 'category' if https://github.com/drush-ops/drush/pull/360 is approved.
    'commandfile' => 'sql',
  );

  return $items;
}

function drush_sql_import($file) {
  if (drush_get_option('drop')) {
    drush_invoke_process('@self', 'sql-drop');
  }

  drush_invoke_process('@self', 'sql-query', array(), array('file' => $file));
}
